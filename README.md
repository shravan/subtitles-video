# subtitles-video

This is a simple python GUI program that adds subtitles to a video using ffmpeg.

Select the video and subtitles(srt, ass, ssa) and click on start.

You can select multiple subtitle files simultaneously.

ffmpeg must be installed and present on path.

No other external dependencies required.
from pathlib import Path
import subprocess
from tkinter import DISABLED, NORMAL, Entry, Tk, Label, Button, filedialog

subtitles = []
video = ""


def open_subtitles():
    files = filedialog.askopenfilenames(
        filetypes=(("Subtitles", "*.srt *.ass *.ssa"), ("All files", "*.*")),
        title="Select subtitle files",
    )
    global subtitles
    subtitles = files


def open_video():
    file = filedialog.askopenfilename(
        filetypes=(("Videos", "*.mp4 *.mkv *.mov *.avi"), ("All files", "*.*")),
        title="Select video",
    )
    global video
    video = file


def add_subtitles(output_file):
    # FFmpeg command to add subtitles
    global video, subtitles
    command = ["ffmpeg", "-i", video]
    for subtitle_file in subtitles:
        command.extend(["-i", subtitle_file])

    command.extend(["-map", "0:v"])
    command.extend(["-map", "0:a"])

    for i in range(1, len(subtitles) + 1):
        command.extend(["-map", f"{i}"])

    command.extend(["-c:v", "copy"])
    command.extend(["-c:a", "copy"])
    command.extend(["-c:s", "mov_text"])
    command.append("-y")
    command.append(output_file)
    subprocess.run(command, check=True)


def main():
    root = Tk()
    root.title("Add subtitles to a video")

    note_text = "This scripts adds subtitles to a video using ffmpeg."
    note_label = Label(root, text=note_text, font=("Arial", 10, "bold"), pady=20)
    note_label.pack()

    video_btn = Button(root, text="Select video", padx=5, pady=10, command=open_video)
    video_btn.pack()

    subtitles_btn = Button(
        root, text="Select subtitles", padx=5, pady=10, command=open_subtitles
    )
    subtitles_btn.pack()

    output_label = Label(
        root, text="Enter output filename (will be overwritten, in the same directory as of original video): ", padx=5, pady=10
    )
    output_label.pack()
    output_entry = Entry(root)
    output_entry.pack()
    output_entry.insert(0, "output.mp4")

    def start():
        try:
            start_btn.configure(state=DISABLED)

            output_file = Path(video).parent.resolve() / output_entry.get().strip()
            Label(root, text=f"Writing to file {output_file}...").pack()
            add_subtitles(output_file)

            Label(root, text="Added subtitles to the video successfully.", fg="green").pack()
            start_btn.configure(state=NORMAL)
        except PermissionError:
            Label(root, text="The video or subtitle files are being accessed by some another program, or this script doesn't have enough permissions to carry on the operation.", fg="red").pack()
        except Exception:
            Label(root, text="An unknown error has occured!", fg="red").pack()

    start_btn = Button(
        root, text="Start", command=start, padx=20, pady=10, borderwidth=3
    )
    start_btn.pack()

    root.mainloop()


if __name__ == "__main__":
    main()
